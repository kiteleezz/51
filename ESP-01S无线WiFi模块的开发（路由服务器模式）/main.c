#include "reg52.h"
#include <intrins.h>
#include <string.h>

sfr AUXR = 0x8E;
sbit LED1 = P3^7;
sbit LED2 = P3^6;

char string_get[5] = {0};
char string_get2[12] = {0};
//char string_get3[10] = {0};// 获取SEND OK反馈
char command[16] = 0;
char flag_IsOk = 0;// 每一步AT指令是否执行成功的标志
char flag_WaitConnection = 0;// 等待PC机连接ESP-01S网络和服务器的标志
char flag_IsSendReady = 0;
//char flag_IsSendOK = 0;
char command_SetCWMODE[] = "AT+CWMODE=3\r\n";
char command_EnableMultipleConnections[] = "AT+CIPMUX=1\r\n";// 使能多连接
char command_EstablishingServices[] = "AT+CIPSERVER=1\r\n";// 建立服务
char command_SendData[] = "AT+CIPSEND=0,48\r\n";// 向第0个通道发送48个字节的内容

void Delay1000ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 8;
	j = 1;
	k = 243;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void Delay3000ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 22;
	j = 3;
	k = 227;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

// 波特率由定时器和计数器作用产生，对于51单片机，主要由定时器1控制，9600
void UartInit()		//9600bps@11.0592MHz
{
	PCON &= 0x7F;		// 波特率不加倍
	SCON = 0x50;		//8位数据，可变波特率
	AUXR &= 0xBF;		
	AUXR &= 0xFE;		
	TMOD &= 0x0F;		//清除定时器1的模式位
	TMOD |= 0x20;		//设置定时器1为8位自动重载模式
	TL1 = 0xFD;		//
	TH1 = 0xFD;		//初值根据波特率计算方式计算得到
	ET1 = 0;		//禁止定时器1中断
	TR1 = 1;		//启动定时器1
}

void LED1Flash()
{
	LED1 = 0;
	Delay1000ms();
	LED1 = 1;
	Delay1000ms();
}

void SendCharacter(char character)
{
	SBUF = character;
	while(TI == 0); // 利用SBUF发出一个字符后，硬件置1，必须由软件清0
	TI = 0;
}

void SendString(char* string)
{
	while(*string != '\0')
	{
		SendCharacter(*string);
		string ++;
	}
}

void main()
{
	EA = 1;
	ES = 1;
  UartInit();	

	Delay3000ms();
	
	SendString(command_SetCWMODE);
	while(flag_IsOk == 0);
	flag_IsOk = 0;
	LED1Flash();
	
	SendString(command_EnableMultipleConnections);
	while(flag_IsOk == 0);
	flag_IsOk = 0;
	LED1Flash();
	
	SendString(command_EstablishingServices);
	while(flag_IsOk == 0);
	flag_IsOk = 0;
	LED1Flash();
	flag_WaitConnection = 1;
	
	while(flag_WaitConnection == 1);
	LED1Flash();

	LED1 = 0;// 连接成功的状态指示灯
	flag_IsSendReady = 1;
	
	while(1)
	{
		SendString(command_SendData);
//		while(flag_IsOk == 0);
//		flag_IsOk = 0;
		Delay1000ms();// 为了不影响单片机接收open、clos指令，这里把反馈做成延时以确保AT指令执行成功
		
		
		SendString("I am sending you a string every three seconds.\r\n");
//		while(flag_IsSendOK == 0);
//		flag_IsSendOK = 0;
		Delay3000ms();// 为了不影响单片机接收open、clos指令，这里把反馈做成延时以确保字符串发送成功
  }
}

void UART_Handler() interrupt 4
{
	char data_get = 0;
	static int i = 0;
	if(RI == 1)
	{
		if(flag_IsSendReady == 0)// AT指令没有全部执行成功之前，用于接收ESP-01S的反馈
		{
			if(flag_WaitConnection == 1)
			{
				data_get = SBUF;
				string_get2[i] = data_get;
				i++;
				if(i == 11 || data_get == '\n') // ESP-01S反馈的字符串为"xxxxOK\r\n"时表示AT指令执行完毕，xxxxxx为4的整数倍个字符
				{
					if(strcmp(string_get2, "0,CONNECT\r\n") == 0)
					{
						flag_WaitConnection = 0;
					}			
					i = 0;
				}	
				RI = 0;
			}
			else
			{
				data_get = SBUF;
				string_get[i] = data_get;
				i++;
				if(i == 4 || data_get == '\n') // ESP-01S反馈的字符串为"xxxxOK\r\n"时表示AT指令执行完毕，xxxxxx为4的整数倍个字符
				{
					if(strcmp(string_get, "OK\r\n") == 0)
					{
						flag_IsOk = 1;
					}			
					i = 0;
				}	
				RI = 0;
			}			
		}
		else// AT指令全部执行完毕之后，用于接收网络调试助手通过WiFi发送过来的命令，注意发指令时open和clos要转行
		{
			data_get = SBUF;
			command[i] = data_get;
			i++;
			if(i == 15 || data_get == '\n') // 传给ESP-01S的字符串为“xxxxxxopen\r\n“时亮灯，灭灯同理，xxxxxx为6的整数倍个字符
			{
				if(strcmp(command, "+IPD,0,6:open\r\n") == 0)
					LED2 = 0;
				if(strcmp(command, "+IPD,0,6:clos\r\n") == 0)
					LED2 = 1;
				i = 0;
			}	
			RI = 0;
		}	
	}
}