#include "reg52.h"
#include "delay.h"
#include <intrins.h>

sbit DHT11 = P1^3;

char data_40bit[5] = {0};
char temp[5] = {0};
char humi[5] = {0};

void GetDHT11Data()
{
	// 看时序图：
	int i = 0;
	int j = 0;
	DHT11 = 1;
	DHT11 = 0;
	Delay30ms();
	DHT11 = 1;
	while(DHT11 == 1);
	while(DHT11 == 0);
	while(DHT11 == 1);
	for(i = 0;i < 5; i++)
	{
		for(j = 0;j < 8; j++)
		{
			while(DHT11 == 0);
			Delay40us();
			
			if(DHT11 == 1)// 数据为1
			{
				data_40bit[i] = data_40bit[i] << 1;
				data_40bit[i] = data_40bit[i] | 0x01;
				while(DHT11 == 1);
			}		
			else // 数据为0
			{
				data_40bit[i] = data_40bit[i] << 1;
				data_40bit[i] = data_40bit[i] & 0xFE;		
			}
		}
	}
}

char* ProcessTempData()// 处理温度数据，并获得温度字符串，只考虑温度整数和小数都是两位数的情况
{
	temp[0] = (data_40bit[2] / 10) + 0x30;
	temp[1] = (data_40bit[2] % 10) + 0x30;
	temp[2] = '.';
	temp[3] = (data_40bit[3] / 10) + 0x30;
	temp[4] = (data_40bit[3] % 10) + 0x30;
	
	return temp;
}
char* ProcessHumiData()// 处理湿度数据，并获得温度字符串，只考虑湿度整数和小数都是两位数的情况
{
	humi[0] = (data_40bit[0] / 10) + 0x30;
	humi[1] = (data_40bit[0] % 10) + 0x30;
	humi[2] = '.';
	humi[3] = (data_40bit[1] / 10) + 0x30;
	humi[4] = (data_40bit[1] % 10) + 0x30;
	
	return humi;
}