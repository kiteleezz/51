#include "delay.h"
#include "LCD1602.h"
#include "DHT11.h"
#include "OLED.h"

void main()
{
	char FirstLine_data[16] = "Temp:xxxxxC     ";
	char SecondLine_data[16] = "Humi:xxxxx%     ";
	char* p_temp;
	char* p_humi;
	int i = 0;
	Delay1000ms(); // 上电稳定2s
	Delay1000ms();
	LCD1602_Initialization();// LCD1602初始化
	OLED_Initialization();// OLED初始化
	OLED_Clear();// OLED清屏
	
	while(1)
	{
		GetDHT11Data();
		p_temp = ProcessTempData();
		p_humi = ProcessHumiData();
		for(i=5;i<=9;i++)
		{		
			FirstLine_data[i] = *p_temp;
			p_temp++;
		}
		for(i=5;i<=9;i++)
		{
			SecondLine_data[i] = *p_humi;
			p_humi++;
		}
		LCD1602_ShowLine(1, FirstLine_data);
		LCD1602_ShowLine(2, SecondLine_data);
		OLED_ShowLine(2, FirstLine_data);
		OLED_ShowLine(3, SecondLine_data);
		Delay500ms();
	}
}