#include "reg52.h"
#include "delay.h"
#include <intrins.h>

#define Data_SBUF P0

sbit RS = P1^0;
sbit RW = P1^1;
sbit EN = P1^2;

void CheckBusySignal() // 检测忙信号
{
	char IsBusy = 0x80;
	P0 = 0x80;
	while(IsBusy & 0x80)
	{
		RS = 0;
		RW = 1;// 写入LCD1602时为0；读为1
		// 根据时序图：
		EN = 0;
		_nop_();
		EN = 1;
		_nop_();
		_nop_();
		IsBusy = Data_SBUF; // P0最高位为1则为忙
		EN = 0;
		_nop_();
	}
}

void WriteData(char show_data)// 写数据
{
	CheckBusySignal(); // 是否在忙
	RS = 1; // 写数据
	RW = 0; // 写入LCD1602时为0；读为1
	// 根据时序图：
	EN = 0;
	_nop_();
	Data_SBUF = show_data;
	_nop_();
	EN = 1;
	_nop_();
	_nop_();
	EN = 0;
	_nop_();
}

void WriteAddress(char address) // 写指令（地址）
{
	CheckBusySignal();// 是否在忙
	RS = 0; // 写地址
	RW = 0; // 写入LCD1602时为0；读为1
	// 根据时序图：
	EN = 0;
	_nop_();
	Data_SBUF = address;
	_nop_();
	EN = 1;
	_nop_();
	_nop_();
	EN = 0;
	_nop_();
}

void LCD1602_Initialization()// 初始化LCD1602
{
	// 延时15ms
	Delay15ms();
	// 写指令38H（不检测忙信号）
	WriteAddress(0x38);
	// 延时5ms
	Delay5ms();
	// 以后每次写指令，读/写数据操作均需要检测忙信号
	// 写指令38H：显示模式设置
	WriteAddress(0x38);
	// 写指令08H：显示关闭
	WriteAddress(0x08);
	// 写指令01H：显示清屏
	WriteAddress(0x01);
	// 写指令06H：显示光标移动设置
	WriteAddress(0x06);
	// 写指令0CH：显示开机光标位置
	WriteAddress(0x0c);
}

void LCD1602_ShowLine(int line, char linedata[16]) // 显示一行数据
{
	char address = 0;
	char i = 0;
	if(line == 1)
	{
		for(i = 0;i < 16;i++)
		{
			address = 0x80 + 0x00 + i;
			WriteAddress(address);
			WriteData(linedata[i]);
		}	
	}
	if(line == 2)
	{
		for(i = 0;i < 16;i++)
		{
			address = 0x80 + 0x40 + i;
			WriteAddress(address);
			WriteData(linedata[i]);
		}	
	}
}