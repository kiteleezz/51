#include "reg52.h"
#include <intrins.h>
#include "OLED8_16array.h"

sbit scl = P2^4;
sbit sda = P2^5;

void IIC_Start() //IIC起始信号
{
	scl = 0;//防止OLED屏的雪花
	sda = 1;
	scl = 1;
	_nop_();// 考虑函数压栈的时间，耗时5us
	sda = 0;
	_nop_();
}

void IIC_Stop() // IIC终止信号
{
	scl = 0;//防止OLED屏的雪花
	sda = 0;
	scl = 1;
	_nop_();
	sda = 1;
	_nop_();
}

char IIC_ACK() // IIC应答信号
{
	char flag = 0;
	sda = 1; // 就在时钟脉冲9期间释放数据线
	_nop_();
	scl = 1;
	flag = sda;
	_nop_();
	scl = 0;
	_nop_();
	
	return flag;
}

void IIC_Send_Byte(char send_data)// IIC发送一个字节
{
	int i = 0;
	for(i=0;i<8;i++)
	{
		scl = 0;// scl拉低，让sda做好数据准备
		sda = send_data & 0x80;// 获得send_data的最高位
		_nop_();// 发送数据建立时间
		scl = 1;// scl拉高开始发送
		_nop_();// 数据发送时间
		scl = 0;// 发送完毕拉低
		_nop_();
		send_data = send_data << 1;
	}
}

void OLED_Write_Command(char command_data)
{
	//看手册，配置OLED分一下几步：
	// statr()
	// 0111 10xx,"x"为设备地址和R/W，这里给第一台设备写入，所以配置成0111 1000
	//ACK
	//control byte:xy00 0000,"x"表示发送控制字节还是数据字节，这里设置为数据字节0，"y"为0写入命令，为1写入数据
	//ACK
	// 写入指令或者命令
	//ACK
	//STOP
	IIC_Start();
	IIC_Send_Byte(0x78);//0111 1000
	IIC_ACK();
	IIC_Send_Byte(0x00);
	IIC_ACK();
	IIC_Send_Byte(command_data);
	IIC_ACK();
	IIC_Stop();
}

void OLED_Write_Data(char show_data)
{
	//看手册，配置OLED分一下几步：
	// statr()
	// 0111 10xx,"x"为设备地址和R/W，这里给第一台设备写入，所以配置成0111 1000
	//ACK
	//control byte:xy00 0000,"x"表示发送控制字节还是数据字节，这里设置为数据字节0，"y"为0写入命令，为1写入数据
	//ACK
	// 写入指令或者命令
	//ACK
	//STOP
	IIC_Start();
	IIC_Send_Byte(0x78);//0111 1000
	IIC_ACK();
	IIC_Send_Byte(0x40);
	IIC_ACK();
	IIC_Send_Byte(show_data);
	IIC_ACK();
	IIC_Stop();
}

void OLED_Initialization()// OLED初始化
{
	// 看手册：
	OLED_Write_Command(0xAE);//--display off
	OLED_Write_Command(0x00);//---set low column address
	OLED_Write_Command(0x10);//---set high column address
	OLED_Write_Command(0x40);//--set start line address  
	OLED_Write_Command(0xB0);//--set page address
	OLED_Write_Command(0x81); // contract control
	OLED_Write_Command(0xFF);//--128   
	OLED_Write_Command(0xA1);//set segment remap 
	OLED_Write_Command(0xA6);//--normal / reverse
	OLED_Write_Command(0xA8);//--set multiplex ratio(1 to 64)
	OLED_Write_Command(0x3F);//--1/32 duty
	OLED_Write_Command(0xC8);//Com scan direction
	OLED_Write_Command(0xD3);//-set display offset
	OLED_Write_Command(0x00);//
	
	OLED_Write_Command(0xD5);//set osc division
	OLED_Write_Command(0x80);//
	
	OLED_Write_Command(0xD8);//set area color mode off
	OLED_Write_Command(0x05);//
	
	OLED_Write_Command(0xD9);//Set Pre-Charge Period
	OLED_Write_Command(0xF1);//
	
	OLED_Write_Command(0xDA);//set com pin configuartion
	OLED_Write_Command(0x12);//
	
	OLED_Write_Command(0xDB);//set Vcomh
	OLED_Write_Command(0x30);//
	
	OLED_Write_Command(0x8D);//set charge pump enable
	OLED_Write_Command(0x14);//
	
	OLED_Write_Command(0xAF);//--turn on oled panel
}

char GetColHighorLow(int i, char flag)// flag:h/l
{
	char col = 8 * i;//第8*i列
	
	char tmp_low = col & 0x0f;
	char col_low = tmp_low;
	
	char tmp_high = ((col & 0xf0) >> 4) & 0x0f;
	char col_high = tmp_high | 0x10;
	
	if(flag == 'h')
		return col_high;
	else
		return col_low;
}

void SendCharData(char send_data, char flag)//flag:0/1；0表示发送前8个数据，1表示发送后8个数据
{
	int i = 0;
	char* p = GetCharArray(send_data);
	if(flag == 0)
	{
		for(i=0;i<8;i++)
			OLED_Write_Data(p[i]);//p[i] = *(p + i)
	}
	if(flag == 1)
	{
		for(i=8;i<16;i++)
			OLED_Write_Data(p[i]);
	}
}

void OLED_ShowLine(int line, char string_data[16])// line表示第几行(共分4行)
{
	int i = 0;
	int j = 0;
	
	//页寻址模式：先发送command0x20，再发送command0x02
	//选择page0：发送command1011 0xxx，xxx设置8个page里面的哪一个
	// 选择哪列，发送command0000 x3x2x1x0,再发送command0001 y3y2y1y0,y3y2y1y0x3x2x1x0表示哪一列
	//显示一个点：发送数据，例如0x08
	
	OLED_Write_Command(0x20);
	OLED_Write_Command(0x02);
	
	for(i=0;i<16;i++)
	{
		for(j=0;j<8;j++)
		{
			OLED_Write_Command(0xB0 + (line - 1) * 2);//选择page(line - 1) * 2
			// 选择列
			OLED_Write_Command(GetColHighorLow(i, 'l'));
			OLED_Write_Command(GetColHighorLow(i, 'h'));
			// 发送该字节的前8个数据
			SendCharData(string_data[i], 0);
			OLED_Write_Command(0xB0 + (line - 1) * 2 + 1);//选择page(line - 1) * 2 + 1
			// 选择列
			OLED_Write_Command(GetColHighorLow(i, 'l'));
			OLED_Write_Command(GetColHighorLow(i, 'h'));
			// 发送该字节的后8个数据
			SendCharData(string_data[i], 1);
		}
	}
}

void OLED_Clear()// 清屏
{
//	int i = 0;
//	int j = 0;
//	for(i=0;i<8;i++)
//	{
//		OLED_Write_Command(0xB0 + 1);//每个Page
//		// 每个Page从第0列开始
//		OLED_Write_Command(0x00);
//		OLED_Write_Command(0x10);
//		for(j=0;j<128;j++)
//		{
//			OLED_Write_Data(0x00);
//		}
//	}
	char clear[16] = "                ";
	OLED_ShowLine(1, clear);
	OLED_ShowLine(2, clear);
	OLED_ShowLine(3, clear);
	OLED_ShowLine(4, clear);
}
