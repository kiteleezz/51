//// 通过HC-08蓝牙模块每隔三秒发送一个字符串的代码：

//#include "reg52.h"
//#include <intrins.h>

//sfr AUXR = 0x8E;

//void Delay3000ms()		//@11.0592MHz
//{
//	unsigned char i, j, k;

//	_nop_();
//	i = 22;
//	j = 3;
//	k = 227;
//	do
//	{
//		do
//		{
//			while (--k);
//		} while (--j);
//	} while (--i);
//}

//// 波特率由定时器和计数器作用产生，对于51单片机，主要由定时器1控制
//void UartInit()		//9600bps@11.0592MHz
//{
//	PCON &= 0x7F;		// 波特率不加倍
//	SCON = 0x50;		//8位数据，可变波特率
//	AUXR &= 0xBF;		
//	AUXR &= 0xFE;		
//	TMOD &= 0x0F;		//清除定时器1的模式位
//	TMOD |= 0x20;		//设置定时器1为8位自动重载模式
//	TL1 = 0xFD;		//
//	TH1 = 0xFD;		//初值根据波特率计算方式计算得到
//	ET1 = 0;		//禁止定时器1中断
//	TR1 = 1;		//启动定时器1
//}

//void SendCharacter(char character)
//{
//	SBUF = character;
//	while(TI == 0); // 利用SBUF发出一个字符后，硬件置1，必须由软件清0
//	TI = 0;
//}

//void SendString(char* string)
//{
//	while(*string != '\0')
//	{
//		SendCharacter(*string);
//		string ++;
//	}
//}

//void main()
//{
//	char data_send[] = "I am sending you a string every three seconds.\r\n";
//	UartInit();
//	while(1)
//	{		
//		SendString(data_send);
//		Delay3000ms();
//	}
//}











//// 通过HC-08蓝牙模块接收“open“、”clos”字符串控制LED灯的亮和灭的代码：

//#include "reg52.h"
//#include <string.h>

//sfr AUXR = 0x8E;
//sbit LED1 = P3^7;

//char string_get[6] = {0};// 避免返回一个局部变量的地址，所以创建为全局变量

//// 波特率由定时器和计数器作用产生，对于51单片机，主要由定时器1控制
//void UartInit()		//9600bps@11.0592MHz
//{
//	PCON &= 0x7F;		// 波特率不加倍
//	SCON = 0x50;		//8位数据，可变波特率
//	AUXR &= 0xBF;		
//	AUXR &= 0xFE;		
//	TMOD &= 0x0F;		//清除定时器1的模式位
//	TMOD |= 0x20;		//设置定时器1为8位自动重载模式
//	TL1 = 0xFD;		//
//	TH1 = 0xFD;		//初值根据波特率计算方式计算得到
//	ET1 = 0;		//禁止定时器1中断
//	TR1 = 1;		//启动定时器1
//}

//char GetCharacter()
//{
//	char data_get = 0;
//	while(RI == 0);// 利用SBUF收到一个字符后，硬件置1，必须由软件清0。	
//	data_get = SBUF;	
//	RI = 0;
//	return data_get;
//}

//char* GetString()
//{
//	char char_get = 0;
//	int i = 0;
//	do
//	{
//		char_get = GetCharacter();
//		string_get[i] = char_get;
//		i ++;
//	}
//	while(char_get != '\n' && i != 6);
//	return string_get;
//}

//void main()
//{
//	int i = 0;
//	char* p_command = NULL;
//	char command[7] = {0};	
//	UartInit();
//	while(1)
//	{	
//		p_command = GetString();
//		for(i = 0;i < 6;i++)
//		{
//			command[i] = p_command[i];
//		}	
//		if(strcmp(command, "open\r\n") == 0)
//			LED1 = 0;
//		if(strcmp(command, "clos\r\n") == 0)
//			LED1 = 1;
//	}
//}










// 结合发送和接收完成中断，同时实现以上两段功能

#include "reg52.h"
#include <intrins.h>
#include <string.h>

sfr AUXR = 0x8E;
sbit LED1 = P3^7;

char command[7] = 0;

void Delay3000ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 22;
	j = 3;
	k = 227;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

// 波特率由定时器和计数器作用产生，对于51单片机，主要由定时器1控制
void UartInit()		//9600bps@11.0592MHz
{
	PCON &= 0x7F;		// 波特率不加倍
	SCON = 0x50;		//8位数据，可变波特率
	AUXR &= 0xBF;		
	AUXR &= 0xFE;		
	TMOD &= 0x0F;		//清除定时器1的模式位
	TMOD |= 0x20;		//设置定时器1为8位自动重载模式
	TL1 = 0xFD;		//
	TH1 = 0xFD;		//初值根据波特率计算方式计算得到
	ET1 = 0;		//禁止定时器1中断
	TR1 = 1;		//启动定时器1
}

void SendCharacter(char character)
{
	SBUF = character;
	while(TI == 0); // 利用SBUF发出一个字符后，硬件置1，必须由软件清0
	TI = 0;
}

void SendString(char* string)
{
	while(*string != '\0')
	{
		SendCharacter(*string);
		string ++;
	}
}

void main()
{
	char data_send[] = "I am sending you a string every three seconds.\r\n";
	EA = 1;
	ES = 1;
	UartInit();
	while(1)
	{		
		SendString(data_send);
		Delay3000ms();
	}
}

void UART_Handler() interrupt 4
{
	char data_get = 0;
	static int i = 0;
	if(RI == 1)
	{
		data_get = SBUF;
		command[i] = data_get;
		i++;
		if(i == 6 || data_get == '\n') // 传给HC-08的字符串为“xxxxxxopen\r\n“时亮灯，灭灯同理，xxxxxx为6的整数倍个字符
		{
			if(strcmp(command, "open\r\n") == 0)
				LED1 = 0;
			if(strcmp(command, "clos\r\n") == 0)
				LED1 = 1;
			i = 0;
		}	
		RI = 0;
	}
}