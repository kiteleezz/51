#include "reg52.h"
#include <intrins.h>

void Delay10us();
void Delay500us();
void Delay19500us();
void Delay1500us();
void Delay18500us();
void Delay2500us();
void Delay17500us();
void Delay50ms();
void Delay3000ms();