#include "main.h"

void Delay10us()		//@11.0592MHz
{
	unsigned char i;

	i = 2;
	while (--i);
}

void Delay500us()		//@11.0592MHz,0.5ms
{
	unsigned char i;

	_nop_();
	i = 227;
	while (--i);
}

void Delay19500us()		//@11.0592MHz,19.5ms
{
	unsigned char i, j;

	_nop_();
	i = 35;
	j = 243;
	do
	{
		while (--j);
	} while (--i);
}

void Delay1500us()		//@11.0592MHz,1.5ms
{
	unsigned char i, j;

	i = 3;
	j = 173;
	do
	{
		while (--j);
	} while (--i);
}

void Delay18500us()		//@11.0592MHz,1.8ms
{
	unsigned char i, j;

	_nop_();
	i = 34;
	j = 39;
	do
	{
		while (--j);
	} while (--i);
}

void Delay2500us()		//@11.0592MHz,2.5ms
{
	unsigned char i, j;

	i = 5;
	j = 120;
	do
	{
		while (--j);
	} while (--i);
}

void Delay17500us()		//@11.0592MHz,17.5ms
{
	unsigned char i, j;

	i = 32;
	j = 93;
	do
	{
		while (--j);
	} while (--i);
}

void Delay50ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 90;
	j = 163;
	do
	{
		while (--j);
	} while (--i);
}

void Delay3000ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 22;
	j = 3;
	k = 227;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}