#include "main.h"

sbit sg90 = P1^4;
sbit HCSR04_Trig = P1^5;
sbit HCSR04_Echo = P1^6;
sbit LED1 = P3^7;

char sg90_speed_flag = 0;// 0代表速度为0，1代表最大正速度，2代表最大负速度
int Timer0_count = 0;
int Timer1_count = 0;
char can_flag = 0;// 垃圾桶状态：0代表关，1代表开

void Timer0Init()		//@11.0592MHz
{
	TMOD &= 0xF0;		//定时器0模式设置
	TMOD |= 0x01;		//定时器0模式设置
	TL0 = 0x00;		//定时器0初值设置
	TH0 = 0x00;		//定时器0初值设置
	TF0 = 0;		//溢出位置0
	TR0 = 0;		//禁止计时
}

void Timer1Init()		//@11.0592MHz,10ms
{
	TMOD &= 0x0F;	
	TMOD |= 0x10;		
	TL1 = 0x00;		
	TH1 = 0xDC;		
	TF1 = 0;		
	TR1 = 0;		
}

void StartHCSR04()
{
	HCSR04_Trig = 0;
	Delay50ms(); // 经过测试在高电平触发超声波传感器（HC-SR04）发出声波之前，最好给一个一定时间的低电平（类似于上升沿触发）
	HCSR04_Trig = 1;
	Delay10us();
	HCSR04_Trig = 0;
}

void sg90_MaximumPositiveSpeed()
{
	while(sg90_speed_flag == 1)
	{
		sg90 = 1;
		Delay500us();
		sg90 = 0;
		Delay19500us();
	};
	can_flag = 1;
}

void sg90_MaximumNegativeSpeed()
{
	while(sg90_speed_flag == 2)
	{
		sg90 = 1;
		Delay2500us();
		sg90 = 0;
		Delay17500us();
	};
	can_flag = 0;
}

void main()
{
	double time = 0;
	double dis = 0;
	int i = 0;
	
	EA = 1; // 打开总中断
	ET0 = 1; // 打开定时器0中断
	ET1 = 1;
	
	Timer0Init();
	Timer1Init();
	
	// 初始化舵机，防止舵机由于刚上电电压不稳而乱动
	for(i=0; i<10;i++)
	{
		sg90 = 1;
		Delay1500us();
		sg90 = 0;
		Delay18500us();
	}
	
	while(1)
	{
		StartHCSR04();
		while(HCSR04_Echo == 0);
		TR0 = 1;
		while(HCSR04_Echo == 1);
		TR0 = 0;
		time = ((TH0 * 256 + TL0) + 65536 * Timer0_count) / (11.0592 * 1e6) * 12;// 单位s
		dis = time * 340 / 2;// 单位m
		if(dis < 0.2)
		{
			LED1 = 0;// 距离小于20cm亮灯
			if(can_flag == 0)
			{
				
				sg90_speed_flag = 1;
				TR1 = 1;
				sg90_MaximumPositiveSpeed();			
			}		
		}
		else
		{
			LED1 = 1;// 距离大于20cm灭灯
			if(can_flag == 1)
			{
				Delay3000ms();// 延迟3s关盖				
				sg90_speed_flag = 2;
				TR1 = 1;
				sg90_MaximumNegativeSpeed();
			}
		}
		TL0 = 0;		
	  TH0 = 0;
		Timer0_count = 0;
	}
}

void Timer0Hanlder() interrupt 1
{
	Timer0_count ++;
}

void Timer1Handler() interrupt 3
{
	Timer1_count ++;
	// 恢复定时器初值
	TL1 = 0x00;		
	TH1 = 0xDC;
	if(Timer1_count == 15)// 开关盖时，控制舵机用最大正（负）速运行150ms
	{
		sg90_speed_flag = 0;
		Timer1_count = 0;
		TR1 = 0; // 停止计时	
	}
}