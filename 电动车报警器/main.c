#include "reg52.h"
#include <intrins.h>

sbit LED1 = P3^7;
sbit LED2 = P3^6;
sbit key_A = P1^4;
sbit key_B = P1^5;
sbit Switcher = P2^0;
sbit shake = P1^6;

void Delay500ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	_nop_();
	i = 4;
	j = 129;
	k = 119;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void NoAlarmMode()
{
	// �Ǿ���״̬����
	LED1 = 1;// ����״ָ̬ʾ����
	LED2 = 0;// �Ǿ���״ָ̬ʾ����
	// �Ǿ���״̬��ʾ����������һ��
	Switcher = 1;
	Delay500ms();
	Switcher = 0;
}

void AlarmMode()
{
	// ����״̬����
	LED1 = 0;// ����״ָ̬ʾ����
	LED2 = 1;// �Ǿ���״ָ̬ʾ����
	// ����״̬��ʾ��������������
	Switcher = 1;
	Delay500ms();
	Switcher = 0;				
	Delay500ms();
	Switcher = 1;
	Delay500ms();
	Switcher = 0;
}

void main()
{
	char mode = 0;// 0�����Ǿ���ģʽ��1��������ģʽ
	LED1 = 1;
	LED2 = 1;	
	Switcher = 0;// �ӽӼ̵��������ˣ�0������·��1������ͨ
	while(1)
	{
		if(key_A==1&&key_B==0)
		{
			AlarmMode();// ����״̬
			mode = 1;
		}
		if(key_A==0&&key_B==1)
		{
			NoAlarmMode();// �Ǿ���״̬
			mode = 0;
		}
		
		if(mode==1)
		{
			if(shake==0)// ��⵽��
			{
				Switcher = 1;
				Delay500ms();
				Switcher = 0;				
				Delay500ms();
				Switcher = 1;
				Delay500ms();
				Switcher = 0;
				Delay500ms();
				Switcher = 1;				
				Delay500ms();
				Switcher = 0;
				Delay500ms();
				Switcher = 1;			
				Delay500ms();
				Switcher = 0;
				Delay500ms();
				Switcher = 1;
				Delay500ms();
				Switcher = 0;				
				Delay500ms();
			  Switcher = 1;
				Delay500ms();
				Switcher = 0;	
			}
		}
	}
}
